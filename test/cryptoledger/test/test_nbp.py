import unittest
from cryptoledger.utils import dump, to_date
from cryptoledger.model import Trade, TradePosition, match_buy_sell
from cryptoledger.nbp import NbpCurrencyConverter


class TestEngine(unittest.TestCase):

    def test_rate(self):
        conv = NbpCurrencyConverter(True)
        self.assertEqual(4.2336, conv.rate(to_date('2017-11-03'), 'EUR', 'PLN'))
        self.assertEqual(4.2498, conv.rate(to_date('2017-11-02'), 'EUR', 'PLN'))
        self.assertEqual(4.424,  conv.rate(to_date('2017-01-01'), 'EUR', 'PLN'))
        self.assertEqual(0.1138,  conv.rate(to_date('2018-07-03'), 'THB', 'PLN'))

        for x in [
            "2017-07-10",
            "2017-07-11",
            "2017-08-16",
            "2017-08-18",
            "2017-11-08"]:
            print conv.rate(x, 'USD', 'PLN')

if __name__ == '__main__':
    unittest.main()