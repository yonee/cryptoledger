from copy import copy, deepcopy
from cryptoledger.utils import to_date, SmartList, is_equal, is_fiat


class CurrencyConverter(object):
    def __init__(self):
        pass

    def rate(self, date, fromCurrency, toCurrency):
        raise Exception("Not implemented!")

    def convert(self, amount, date, fromCurrency, toCurrency):
        return amount * self.rate(date, fromCurrency, toCurrency)


class Trade(object):
    def __init__(self, id, currency1, currency2, is_buy, price, volume, fee, date):
        '''
            buying/selling currency 2 with currency1
            :param id:
            :param id:
        '''
        self.__dict__.update(locals())
        self.date = to_date(date)
        self.year_month = "%04d-%02d" % (self.date.year, self.date.month)
        self.year_quarter = "%04d-Q%d" % (self.date.year, 1 + (self.date.month-1)//3)
        self.is_sell = not self.is_buy

    def is_fiat(self):
        return is_fiat(self.currency1) or is_fiat(self.currency2)

    def reverse(self):
        self.currency1, self.currency2 = self.currency2, self.currency1
        self.volume = self.price * self.volume
        self.price = 1.0 / self.price
        self.is_buy, self.is_sell = self.is_sell, self.is_buy

    def __str__(self):
        self.pair = "%(currency1)s%(currency2)s" % self.__dict__
        self.oper = "sell" if self.is_sell else "buy "
        return "%(id)s: %(date)s %(oper)s %(pair)s %(price)lf x %(volume)lf [%(fee)lf]" % self.__dict__

    def __eq__(self, other):
        result = isinstance(other, Trade) and \
               self.date == other.date and \
               self.currency1 == other.currency1 and \
               self.currency2 == other.currency2 and \
               self.is_buy == other.is_buy and \
               is_equal(self.price, other.price) and \
               is_equal(self.volume, other.volume) and \
               is_equal(self.fee, other.fee)
        if not result:
            print "Objects to not match:\n- %s\n- %s" % (self, other)
        return result

    def __getitem__(self, item):
        return self.__dict__[item]

    def split(self, volume):
        if volume >= self.volume:
            raise Exception("Split %ld bigger then volume of trade %s" % (volume, self))
        totalVolume = self.volume
        trade2 = copy(self)
        trade2.volume = self.volume - volume
        trade2.fee = trade2.fee * trade2.volume / totalVolume
        self.volume = volume
        self.fee = self.fee * self.volume / totalVolume
        return trade2

    def value(self):
        return self.price * self.volume

class TradePosition:

    def __init__(self, openTrade, closeTrade):
        self.__dict__.update(locals())

    def profit(self):
        return self.closeTrade.value() - self.openTrade.value()

    def __getitem__(self, item):
        return self.__dict__[item]

    def __str__(self):
        return "%(openTrade)s => %(closeTrade)s" % self.__dict__

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.openTrade.__eq__(other.openTrade) and self.closeTrade.__eq__(other.closeTrade)


class Rate:
    def __init__(self, date, value, comment):
        date = to_date(date)
        self.__dict__.update(locals())


def test_buy_and_currency(c):
    return lambda x: x.currency1 == c and x.is_buy


def process_splits(iter1, iter2):
    while iter2.current.volume < iter1.current.volume:
        iter1.insert(iter1.current.split(iter2.current.volume))

    while iter2.current.volume > iter1.current.volume:
        iter2.insert(iter2.current.split(iter1.current.volume))
    return iter1.current, iter2.current

def match_buy_sell(trades):
    """
        Matches buy and sell trades using FIFO algorithm and splitting both of them into chunks when needed to
        match their counterparts
        Then builds a collection of positions out of matched pairs.
    """
    trades = SmartList(deepcopy(trades))

    positions = []

    buy_currencies = set([x.currency1 for x in trades.data if x.is_buy])
    buy_iterators = {c: trades.iterator(test_buy_and_currency(c), c) for c in buy_currencies}
    sell_iter = trades.iterator(lambda x: x.is_sell)

    while sell_iter.has_next():
        sell = sell_iter.next()

        buy_iter = buy_iterators[sell.currency1]
        buy = buy_iter.next()

        process_splits(buy_iter, sell_iter)

        positions.append(TradePosition(buy, sell))

    print "Buy iterators status:"
    for i in buy_iterators.values():
        balance = 0
        for x in i:
            balance = balance + x.volume
        print "- %s: %lf" % (i.extra_data, balance)

    return positions


def resolve_non_fiat(trades):
    """
        Resolves non-fiat trades (crypto->crypto)
    """
    trades = SmartList(deepcopy(trades))

    buy_currencies = set([x.currency1 for x in trades.data if x.is_buy])
    buy_iterators = {c: trades.iterator(test_buy_and_currency(c), c) for c in buy_currencies}
    curr_iter = trades.iterator()

    while curr_iter.has_next():
        curr = curr_iter.next()

        if curr.is_fiat():
            if curr.is_sell:
                # normal sell for fiat

                other_iter = buy_iterators[curr.currency1]
                other_iter.next()
                process_splits(curr_iter, other_iter)
        else:
            if curr.is_sell:
                curr.reverse()

            # exchanging cryptos
            # buying crypto A (curr.currency1) for crypto B (curr.currency2)
            # we have to insert dummy sell trades of B and adjust original
            # trades as if they were for fiat

            # calculate volume of crypto B

            volume_b = curr.volume * curr.price

            while volume_b > 0:
                other_iter = buy_iterators[curr.currency2]
                other = other_iter.next()

                if other.volume > volume_b:
                    other_iter.insert(other.split(volume_b))

                new_trade = Trade(other.id, other.currency1, other.currency2, other.is_sell, other.price, other.volume, 0, curr.date)
                curr_iter.insert_before(new_trade)
                if other.volume < volume_b:
                    volume_a = curr.volume * other.volume / volume_b
                    curr_iter.insert(curr.split(volume_a))

                    curr.currency2 = other.currency2
                    curr.price = other.value() / curr.volume

                    curr = curr_iter.next()
                else:
                    curr.currency2 = other.currency2
                    curr.price = other.value() / curr.volume

                volume_b = volume_b - other.volume

    return trades.data