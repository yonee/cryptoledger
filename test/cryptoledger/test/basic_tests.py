import unittest
from cryptoledger.utils import dump, is_equal
from cryptoledger.model import Trade, TradePosition, match_buy_sell, resolve_non_fiat


class TestEngine(unittest.TestCase):

    def test_match_buy_sell(self):
        print "test_match_buy_sell"
        self.maxDiff = None
        result = match_buy_sell([
            Trade("", 'XBT', 'EUR', True, 1.2, 10, 10, '2017-01-01'),
            Trade("", 'ETH', 'EUR', True, 100, 1, 0, '2017-01-01'),
            Trade("", 'XBT', 'EUR', True, 1.3, 10, 10, '2017-01-01'),
            Trade("", 'LTC', 'EUR', True, 100, 1, 0, '2017-01-01'),
            Trade("", 'XBT', 'EUR', True, 1.4, 10, 10, '2017-01-01'),
            Trade("", 'LTC', 'EUR', True, 100, 1, 0, '2017-01-01'),
            Trade("", 'XBT', 'EUR', False, 1.5, 15, 15, '2017-01-02'),
            Trade("", 'LTC', 'EUR', True, 100, 1, 0, '2017-01-01'),
            Trade("", 'XBT', 'EUR', False, 1.6, 12, 12, '2017-01-03'),
            Trade("", 'LTC', 'EUR', True, 100, 1, 0, '2017-01-01'),
            Trade("", 'ETH', 'EUR', False, 120, 0.5, 0, '2017-01-04'),
        ])

        expected = [
            TradePosition(Trade("", 'XBT', 'EUR', True, 1.2, 10, 10, '2017-01-01'),
                          Trade("", 'XBT', 'EUR', False, 1.5, 10, 10, '2017-01-02')),
            TradePosition(Trade("", 'XBT', 'EUR', True, 1.3, 5, 5, '2017-01-01'),
                          Trade("", 'XBT', 'EUR', False, 1.5, 5, 5, '2017-01-02')),
            TradePosition(Trade("", 'XBT', 'EUR', True, 1.3, 5, 5, '2017-01-01'),
                          Trade("", 'XBT', 'EUR', False, 1.6, 5, 5, '2017-01-03')),
            TradePosition(Trade("", 'XBT', 'EUR', True, 1.4, 7, 7, '2017-01-01'),
                          Trade("", 'XBT', 'EUR', False, 1.6, 7, 7, '2017-01-03')),
            TradePosition(Trade("", 'ETH', 'EUR', True, 100, 0.5, 0, '2017-01-01'),
                          Trade("", 'ETH', 'EUR', False, 120, 0.5, 0, '2017-01-04')),
        ]

        #dump(result)
        self.assertListEqual(result, expected)

    def test_resolve_non_fiat1(self):
        print "test_resolve_non_fiat1"
        result = resolve_non_fiat([
            Trade("", 'XBT', 'EUR', True, 1000.00, 1.0, 11, '2017-01-01'),
            Trade("", 'ETH', 'XBT', True, 0.10, 10.0, 12, '2017-01-02'),
            Trade("", 'ETH', 'EUR', False, 200.00, 10.0, 13, '2017-01-03'),
        ])
        expected = [
            Trade("", 'XBT', 'EUR', True, 1000.0, 1.0, 11, '2017-01-01'),
            Trade("", 'XBT', 'EUR', False, 1000.0, 1.0, 0, '2017-01-02'),
            Trade("", 'ETH', 'EUR', True, 100.0, 10.0, 12, '2017-01-02'),
            Trade("", 'ETH', 'EUR', False, 200.0, 10.0, 13, '2017-01-03'),
        ]
        dump(result)
        self.assertListEqual(result, expected)

        expected = [
            TradePosition(Trade("", 'XBT', 'EUR', True, 1000, 1, 11, '2017-01-01'), Trade("", 'XBT', 'EUR', False, 1000, 1, 0, '2017-01-02')),
            TradePosition(Trade("", 'ETH', 'EUR', True, 100, 10, 12, '2017-01-02'), Trade("", 'ETH', 'EUR', False, 200, 10, 13, '2017-01-03')),
        ]
        result = match_buy_sell(result)
        self.assertListEqual(result, expected)

    def test_resolve_non_fiat2(self):
        print "test_resolve_non_fiat2"
        result = resolve_non_fiat([
            Trade("1 ", 'ETH', 'EUR', True,  200.0, 2.0, 11, '2017-01-01'),  # value = 400EUR
            Trade("2 ", 'ETH', 'XBT', False,   0.1, 2.0, 12, '2017-01-02'),  # value = 0.2XBT
        ])
        expected = [
            Trade("1 ", 'ETH', 'EUR', True,  200.0, 2.0, 11, '2017-01-01'),
            Trade("2a", 'ETH', 'EUR', False, 200.0, 2.0,  0, '2017-01-02'),
            Trade("2b", 'XBT', 'EUR', True, 2000.0, 0.2, 12, '2017-01-02'),
        ]
        dump(result)
        self.assertListEqual(result, expected)


    def test_resolve_non_fiat3(self):
        print "test_resolve_non_fiat3"
        result = resolve_non_fiat([
            Trade("1 ", 'XBT', 'EUR', True, 500.0, 0.5, 0, '2017-01-01'),    #1 0.5XBT |
            Trade("2 ", 'XBT', 'EUR', True, 550.0, 0.2, 2, '2017-01-01'),    #2 1.0XBT |
            Trade("3 ", 'XBT', 'EUR', True, 600.0, 0.3, 0, '2017-01-01'),    #3 1.0XBT |
            Trade("4 ", 'XBT', 'EUR', True, 650.0, 2.0, 20, '2017-01-01'),   #4 3.0XBT |
            Trade("5 ", 'XBT', 'EUR', False, 700.0, 0.6, 0, '2017-01-02'),    #5 2.4XBT | 0.5 * (700EUR-500EUR) + 0.1 * (700EUR - 600EUR)
            Trade("6 ", 'ETH', 'XBT', True, 0.1, 5.0, 0, '2017-01-02'),    #6 1.9XBT | 0.4XBT @600EUR * 0.1 + 0.1XBT @ 650EUR * 0.1 = 4ETC @ 60EUR + 1ETC @65EUR
            #Trade('2017-01-02', 'XBT', 'EUR', False, 900.1, 1.0, 0),    # 1.8XBT | 0.4XBT @600EUR * 0.1 + 0.1XBT @ 650EUR * 0.1 = 4ETC @ 60EUR + 1ETC @65EUR
            #Trade('2017-01-02', 'ETH', 'XBT', True,    0.1, 1.0, 0),    # 1.8XBT | 0.1XBT @650EUR
            #Trade('2017-01-03', 'XBT', 'EUR', False, 600.0, 0.5, 0),    # 1.3XBT | 0.5 * (600EUR-650EUR)
            #Trade('2017-01-04', 'ETH', 'EUR', False,    79, 1.0, 0),    # 0.5XBT | 0.5 * (80EUR - 0.1XBT @ 600EUR) = 0.5 * (80EUR-60EUR)
            #Trade('2017-01-04', 'ETH', 'EUR', False,    80, 5.0, 0),    # 0.5XBT | 0.5 * (80EUR - 0.1XBT @ 600EUR) = 0.5 * (80EUR-60EUR)
        ])

        expected = [
            Trade("1 ", 'XBT', 'EUR', True, 500.0, 0.5, 0, '2017-01-01'),    #1
            Trade("2a", 'XBT', 'EUR', True, 550.0, 0.1, 1, '2017-01-01'),    #2a
            Trade("2b", 'XBT', 'EUR', True, 550.0, 0.1, 1, '2017-01-01'),    #2b
            Trade("3 ", 'XBT', 'EUR', True, 600.0, 0.3, 0, '2017-01-01'),    #3
            Trade("4a", 'XBT', 'EUR', True, 650.0, 0.1, 1, '2017-01-01'),    #4a
            Trade("4b", 'XBT', 'EUR', True, 650.0, 1.9, 19, '2017-01-01'),   #4b
            #Trade('2017-01-01', 'XBT', 'EUR', True,  650.0, 0.9, 19),
            Trade("5a", 'XBT', 'EUR', False, 700.0, 0.5, 0, '2017-01-02'),    #5a  #1
            Trade("5b", 'XBT', 'EUR', False, 700.0, 0.1, 0, '2017-01-02'),    #5b  #2a

            Trade("6a", 'XBT', 'EUR', False, 550.0, 0.1, 0, '2017-01-02'),    #6a  #2b         55EUR
            Trade("6x", 'ETH', 'EUR', True, 55.0, 1.0, 0, '2017-01-02'),    #6a

            Trade("6b", 'XBT', 'EUR', False, 600.0, 0.3, 0, '2017-01-02'),    #6b  #3         180EUR
            Trade("6y", 'ETH', 'EUR', True, 60.0, 3.0, 0, '2017-01-02'),    #6b

            Trade("6c", 'XBT', 'EUR', False, 650.0, 0.1, 0, '2017-01-02'),    #6c  #4a         65EUR
            Trade("6z", 'ETH', 'EUR', True, 65.0, 1.0, 0, '2017-01-02'),    #6c
            #Trade('2017-01-02', 'XBT', 'EUR', False, 900.1, 1.0, 0),

            #Trade('2017-01-02', 'XBT', 'EUR', False, 650.0, 0.1, 0),    # 1.9XBT
            #Trade('2017-01-02', 'ETH', 'EUR', True,   65.0, 1.0, 0),    #

            #Trade('2017-01-02', 'ETH', 'XBT', True,    0.1, 1.0, 0),    # 1.8XBT | 0.1XBT @650EUR
            #Trade('2017-01-03', 'XBT', 'EUR', False, 600.0, 0.5, 0),    # 1.3XBT | 0.5 * (600EUR-650EUR)
            #Trade('2017-01-04', 'ETH', 'EUR', False,    79, 1.0, 0),    # 0.5XBT | 0.5 * (80EUR - 0.1XBT @ 600EUR) = 0.5 * (80EUR-60EUR)
        ]
        dump(result)
        self.assertListEqual(result, expected)


if __name__ == '__main__':
    unittest.main()