from datetime import date, datetime, timedelta
from urllib2 import urlopen
from model import Rate, CurrencyConverter
from cryptoledger.utils import to_midnight, format_ymd, to_date
from os.path import exists
import json
import logging

"""
    Loads data from cache file
"""
CACHE_FILE_NAME = ".nbp-cache.json"
cache = {}
if exists(CACHE_FILE_NAME):
    with open(CACHE_FILE_NAME, "r") as f:
        cache = json.load(f)


def loadRate(reqDate, currency):
    """
    Loads a rate from a NBP site for an exact date with caching
    :param reqDate:
    :param currency:
    :return:
    """
    global cache
    currency = currency.lower()
    year = reqDate.year
    today = date.today()
    todayStr = format_ymd(today)
    key = "%(currency)s-%(year)d" % locals()
    if key not in cache or cache[key]["lastDate"] < todayStr:
        endM, endD = (today.month, today.day) if year == today.year else (12, 31)
        url = 'http://api.nbp.pl/api/exchangerates/rates/a/%(currency)s/%(year)04d-01-01/%(year)04d-%(endM)02d-%(endD)02d/' % locals()
        logging.info("Loading " + url)
        data = json.loads(urlopen(url).read().decode('utf-8'))
        data = {x['effectiveDate']: {"rate": x['mid'], "comment": x['no']} for x in data['rates']}
        data["lastDate"] = todayStr
        cache[key] = data
        # update file cache
        with open(CACHE_FILE_NAME, "w") as f:
            json.dump(cache, f, indent=4, sort_keys=True)
    result = cache[key].get(format_ymd(reqDate), None)
    if result:
        return Rate(reqDate, result["rate"], result["comment"])
    else:
        return None

def findRate(reqDate, currency):
    if type(reqDate) is str:
        reqDate = to_date(reqDate)
    reqDate = to_midnight(reqDate)
    for x in range(1, 10):
        reqDate = reqDate - timedelta(days=1)
        rate = loadRate(reqDate, currency)
        if rate:
            return rate
    logging.warn("Could not find %(currency)s rate for %(reqDate)s" % locals())
    return None

def rate(reqDate, fromCurrency, toCurrency, lastBefore):
    return findRate(reqDate, fromCurrency).value


class NbpCurrencyConverter(CurrencyConverter):

    def __init__(self, lastBefore):
        super(NbpCurrencyConverter, self).__init__()
        self.lastBefore = lastBefore

    def rate(self, date, fromCurrency, toCurrency):
        if toCurrency != 'PLN':
            raise Exception("%(toCurrency)s is not supported" % locals())
        return rate(date, fromCurrency, toCurrency, self.lastBefore)