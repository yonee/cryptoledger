from utils import render
from model import match_buy_sell
from sources.kraken import parse_kraken_trades
from nbp import NbpCurrencyConverter
from logging import DEBUG
import logging

logging.root.level = DEBUG

# parser
# matcher (sell&buy if needed)
# transactions


def process_kraken(fileName, pair):
    data = parse_kraken_trades(fileName)

    curTrades = filter(lambda x: x.pair == pair,  data)
    return match_buy_sell(curTrades)


if __name__ == '__main__':
    positions = process_kraken("/Users/yonee/Downloads/kraken-trades-20180723.csv", "XBTEUR")
    render(
        'simple_report.html',
        'output/test.html',
        positions=positions,
        target_currency='PLN',
        converter=NbpCurrencyConverter(True)
    )
