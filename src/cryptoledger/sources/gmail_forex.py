import itertools
from os.path import join

from cryptoledger.nbp import findRate
from cryptoledger.utils import num, parseCSV, to_date, profit_loss, not_empty

import logging
from lxml import etree

def parse_gmail_monthly_statement(file_name):
    '''
    Parses gmail printout
    :param file_name: name of CSV file to parse
    :return: list of Trade objects
    '''

    tree = etree.parse(file_name, etree.HTMLParser())
    tree.findall("//td/b/")
    print tree

def parse_expenses(file_name):
    def parse_line(row):
        rate = findRate(to_date(row["Date"]), "USD").value
        value = rate * num(row["Amount"])
        print value
        return value

    return parseCSV(file_name, parse_line)

def parse_metatrader(file_name):

    def parse_line(row):
        if row["Type"] not in ["buy", "sell"]:
            return None
        rate = findRate(to_date(row["Close Time" if "Close Time" in row else "Open Time"]), "USD").value
        return [
            rate * num(row["Commission"]),
            rate * num(row["Swap" if "Swap" in row else "R/O Swap"]),
            rate * num(row["Profit" if "Profit" in row else "Trade P/L"])]

    return list(itertools.chain.from_iterable(filter(not_empty, parseCSV(file_name, parse_line))))

if __name__ == '__main__':
    dir = "/Users/yonee/Dropbox/docs/podatki/2018/inwestycje"

    print
    result = reduce(profit_loss, parse_metatrader(join(dir, "mt4.csv")) + parse_metatrader(join(dir, "mt5.csv")))
    print "\t".join(map(str, [result[0], result[1], result[0] + result[1]]))

    print
    #parse_expenses(join(dir, "expenses.csv"))


    for x in [
        "2018-12-14",
    ]:
        print str(findRate(to_date(x), "USD").value).replace(".", ",")