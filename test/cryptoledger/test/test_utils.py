import unittest
from cryptoledger.utils import is_equal, SmartList


class TestEngine(unittest.TestCase):

    def test_is_equal(self):
        self.assertEqual(True,  is_equal(0, 0.0))
        self.assertEqual(True,  is_equal(0, 0.0000000009))
        self.assertEqual(False, is_equal(0, 0.000001))
        self.assertEqual(True,  is_equal(-999999, -999999.001))

    def test_smart_list(self):
        l = SmartList([1,2,3,4,5])
        iter1 = l.iterator()
        iter2 = l.iterator(lambda x: x % 2 == 0)
        iter3 = l.iterator(lambda x: x % 2 != 0)


        self.assertEqual(iter1.next(), 1)  # => 2
        self.assertEqual(iter1.next(), 2)  # => 3
        self.assertEqual(iter1.next(), 3)  # => 4
        self.assertEqual(iter1.next(), 4)  # => 5
        self.assertEqual(iter2.next(), 2)  # => 4
        self.assertEqual(iter3.next(), 1)  # => 3
        iter2.insert(100)
        self.assertListEqual(l.data, [1, 2, 100, 3, 4, 5])
        self.assertEqual(iter1.next(), 5)  # => None
        self.assertFalse(iter1.has_next())
        self.assertEqual(iter2.next(), 100)  # => 4
        self.assertEqual(iter3.next(), 3)    # => 5
        self.assertEqual(iter3.next(), 5)  # => None
        iter2.insert(101)
        self.assertEqual(iter2.next(), 4)  # => None
        self.assertFalse(iter2.has_next())


if __name__ == '__main__':
    unittest.main()