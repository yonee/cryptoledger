# -*- coding: utf-8 -*-
from numpy import double
from datetime import datetime
from jinja2 import Environment, PackageLoader
import dateutil.parser

def not_empty(a):
    return a is not None and a != ""

def profit_loss(a, b):
    p, l = a if type(a) is list else profit_loss([0, 0], a)
    return [p + b, l] if b > 0 else [p, l + b]

def is_equal(x1, x2, max_distance=None):
    '''
    Compare two numbers with a given difference margin
    :param x1:
    :param x2:
    :param max_distance: maximum accepted distance (or 0.000001 of the lower number if not specified)
    :return: True if difference is less then max_distance
    '''
    if not max_distance:
        max_distance = max(0.000001 * min(abs(x1), abs(x2)), 0.000000001)
    return abs(x1 - x2) < max_distance

def is_fiat(currency):
    '''
    Determines if a given currency is fiat currency or crypto
    :param currency:
    :return: True for fiat currencies
    '''
    return currency.upper() in ['THB', 'USD', 'AUD', 'HKD', 'CAD', 'NZD', 'SGD', 'EUR', 'HUF', 'CHF', 'GBP', 'UAH', 'JPY', 'CZK', 'DKK', 'ISK', 'NOK', 'SEK', 'HRK', 'RON', 'BGN', 'TRY', 'ILS', 'CLP', 'MXN', 'PHP', 'ZAR', 'BRL', 'MYR', 'RUB', 'IDR', 'INR', 'KRW', 'CNY', 'XDR']

def num(x):
    if type(x) in [str, unicode]:
        x = x.replace(' ', '')
    return double(x)

def to_date(x):
    '''
    Parses date if given argument is string
    :param x: input date
    :return: parsed date if it's a string, original value otherwise
    '''
    return dateutil.parser.parse(x) if type(x) == str or type(x) == unicode else x


def format_ymd(date):
    return "%04d-%02d-%02d" % (date.year, date.month, date.day)


def to_midnight(date):
    return datetime(date.year, date.month, date.day)


def dump(data, msg=None):
    if msg:
        print(msg)
    for x in data:
        print(x)


class SmartList():
    '''
    An implementation of list which cooperates with instances of Iterator class so that inserting objects to a list
    during iteration is properly handled accross all active iterators
    '''
    def __init__(self, data):
        self.iterators = []
        self.data = data

    def iterator(self, filter_function = lambda x: True, extra_data = None): # = lambda x: True):
        i = Iterator(self, filter_function, extra_data)
        self.iterators.append(i)
        return i

    def insert(self, pos, object):
        self.data.insert(pos, object)
        # increment pointers for all iterators pointing ahead of the inserted objects
        for i in self.iterators:
            if pos < i.iter:
                i.iter = i.iter + 1
        return object

class Iterator(object):
    '''
    Smart iterator designed to work with #SmartList. Multiple iterators based on the same
    smart list can work in parallel including runtime list modifications using #insert, #insert_before methods.
    '''

    def __init__(self, smart_list, filter_function, extra_data):
        self.smart_list = smart_list
        self.filter_function = filter_function
        self.extra_data = extra_data
        self.iter = 0
        self.current = None

    def __iter__(self):
        return self

    def find_next(self):
        i = self.iter
        while i < len(self.smart_list.data):
            if self.filter_function(self.smart_list.data[i]):
                break
            i = i + 1
        return i

    def has_next(self):
        return self.find_next() < len(self.smart_list.data)
    
    def next(self):
        self.iter = self.find_next()
        if self.iter >= len(self.smart_list.data):
            msg = "%s: we run out of %s    %d of %d" % (self.current.id, self.extra_data, self.iter, len(self.smart_list.data))
            self.current = None
            raise StopIteration(msg)
        self.current = self.smart_list.data[self.iter]
        self.iter = self.iter + 1
        return self.current

    def insert(self, obj):
        return self.smart_list.insert(self.iter, obj)

    def insert_before(self, obj):
        return self.smart_list.insert(self.iter-1, obj)


def parseCSV(name, transform = None):
    
    def parseLine(input):
        if type(input) is file:
            input = input.readline().decode('utf-8')
        return [x.strip(u'\xa0"\n\r ') for x in input.split(',')]

    def fixColumnDuplicates(columns):
        for x in range(1, len(columns)):
            if columns[x] in columns[:x-1]:
                columns[x] = columns[x] + '2'

        return columns

    with open(name, "r") as f:
        columns = fixColumnDuplicates(parseLine(f))
        rows = []
        for x in f:
            cells = parseLine(x.decode('utf-8'))
            row = {key: cells[idx] for idx, key in enumerate(columns)}
            if transform:
                row = transform(row)
            rows.append( row )
        return rows


def render(src_file, dst_file, **data):

    def formatYMD(date):
        return format_ymd(date)

    def convert(value, date, fromCurrency, toCurrency=data['target_currency']):
        return data['converter'].convert(value, date, fromCurrency, toCurrency)

    def rate(value, date, fromCurrency, toCurrency=data['target_currency']):
        return data['converter'].rate(date, fromCurrency, toCurrency)

    def decimal(value, digits):
        return '%.*f' % (digits, value)

    env = Environment(loader=PackageLoader('cryptoledger', 'templates'))
    env.filters['formatYMD'] = formatYMD
    env.filters['convert'] = convert
    env.filters['rate'] = rate
    env.filters['decimal'] = decimal
    env.get_template(src_file).stream(data).dump(dst_file)
