# cryptoledger

The purpose of the project is to provide a set of tools to simplify making tax calculations. 

The idea originated from the need of calculating taxes on the income from crypto-currencies trading. 
Due to the requirements of the Polish taxation system every transaction needs to be converted from the account currency to Polish Złoty based on the exchange rate from the last working day preceding the transaction date.
Another challenge was a requirement to match buy and sell transaction using FIFO method and then separately covert the open and close prices for each position.

The project is in the early development phase.
