from cryptoledger.utils import num, parseCSV
from cryptoledger.model import Trade
import re
import logging


def parse_kraken_trades(file_name):
    '''
    Parses Kraken trades from CSV format
    :param file_name: name of CSV file to parse
    :return: list of Trade objects
    '''

    def parse_line(trade):
        pair = trade['pair']
        x = re.match("[XZ](...)[XZ](...)", pair)
        if not x:
            x = re.match("(...)(...)", pair)
        if not x:
            logging.error("Ignoring unrecognized currency %(pair)s" % (trade))
            return None

        currency1, currency2 = x.groups()
        return Trade(
            id=trade['txid'],
            currency1 = currency1,
            currency2 = currency2,
            is_buy=("buy" == trade['type']),
            price=num(trade['price']),
            volume=num(trade['vol']),
            fee=num(trade['fee']),
            date=trade['time'])

    return filter(lambda x: x is not None, parseCSV(file_name, parse_line))

